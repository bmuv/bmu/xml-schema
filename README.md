<!--
SPDX-FileCopyrightText: 2023 Consist ITU Software Solutions GmbH

SPDX-License-Identifier: EUPL-1.2
-->

# XML Schema



## Was finden Sie hier?

Die BMU-Schnittstelle definiert die XML-Strukturen für den Nachrichtenaustausch im elektronischen Abfallnachweisverfahren gemäß Nachweisverordnung (NachwV).

Die offizielle Version wird auch unter

https://www.bmuv.de/download/datenschnittstelle-zur-nachweisverordnung

bereitgestellt (Stand des Links: 06/2023).

## Dokumentation der Schnittstelle

Die Dokumentation der Schnittstelle findet sich als PDF-Dokument in diesem Verzeichnis.

Sie ist ebenfalls unter dem oben angegebenen Link veröffentlicht.

## Wie können Sie Fragen, Anregungen oder Kommentare zur Schnittstellendefinition äußern?

Schnittstelle wird fachlich und technisch im Auftrag des BMUV betreut durch

Consist ITU GmbH
Jakobikirchhof 8
20095 Hamburg

Es ist vorgesehen, in diesem Repository zum XML Schema der BMU-Schnittstelle auch Möglichkeiten zu eröffnen, dass Interessierte Fragen, Anregungen oder Kommentate zur Schnittstelle abgeben können. Aktuell ist dies aber noch nicht eingerichtet. Sie können sich übergangsweise - vorzugsweise per E-Mail - an den nachfolgenden Kontakt wenden.

Ihr Ansprechpartner

Eckhard Flor
Mail: eckhard.flor@consist-itu.de
Fon: 040/30625-123

